// console.log("Hello Zawarudo!");
/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/





// #1.a 
function sumTwoNum(num1, num2){
		let sumOfTwo = num1 + num2;
		console.log("Displayed sum of " + num1 + " and " + num2);
		console.log(sumOfTwo);
	} 		

// #1.b 
function diffTwoNum(num1, num2){
		let diffOfTwo = num1 - num2;
		console.log("Displayed sum of " + num1 + " and " + num2);
		console.log(diffOfTwo);
	} 	

// #1.c 
	sumTwoNum(25, 5);	
	diffTwoNum(25, 5);



// #2.a 
function prodTwoNum(num1, num2){
	console.log("The product of " + num1 + " and " + num2);
	let prodOfTwo = num1 * num2;
	console.log(prodOfTwo);
	return prodOfTwo;

	} 		

// #2.b 
function quoTwoNum(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2);
	let quoOfTwo = num1 / num2;
	console.log(quoOfTwo);
	return quoOfTwo;

	}

// #2.c
let product = prodTwoNum(10, 9);
let qoutient = quoTwoNum(10, 9);



// #3.a
function areaCircle(radius){
	let area = 3.14 * Math.pow(radius,2);
	console.log("The result of getting the area of a circle with 30 radius: ");
	console.log(area);
	return area;

	}
let circleArea = areaCircle(30);
	
// #4.a
function averageOfNums(num1, num2, num3, num4){
	let sumOfFourNums = num1 + num2 + num3 + num4;
	let average = (sumOfFourNums / 7);
	console.log("The avaerage of 20, 44, 26, and 76: ");
	console.log(average);
	return average;

	}
let averageVar = averageOfNums(20, 44, 26, 76);

// #5.a
function percentageScore(score, maxScore){
	let percentage = (score / maxScore) * 100;
	let isPassed = percentage > 75;
	console.log("Is 85/100 a passing score?");
	console.log(isPassed);
	return isPassed;

	}
let isPassingScore = percentageScore(85, 100);

